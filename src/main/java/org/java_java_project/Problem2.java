package org.java_java_project;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


//    2. Number of matches won of all teams over all the years of IPL.
public class Problem2 {

    public static Map findWonMatchesByEachTeam(List<Match_Data> matches){
        Map<String, Integer> winMatches = new HashMap<>();
        for(Match_Data index : matches){
            if(winMatches.containsKey(index.getSeason() +" "+ index.getWinner())){
                int winCount = winMatches.get(index.getSeason() +" "+ index.getWinner());
                winMatches.replace(index.getSeason() +" "+ index.getWinner(), winCount+1);
            }
            else{
                winMatches.put(index.getSeason() +" "+ index.getWinner(), 1);
            }
        }
        return winMatches;
    }

}
