package org.java_java_project;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Ipl {

    private static  int id=0;
    private static int season=1;
    private static final int city=2;
    private static final int date=3;
    private static final int team1=4;
    private static final int team2=5;
    private static final int toss_winner=6;
    private static final int toss_decision=7;
    private static final int result=8;
    private static final int dl_applied=9;
    private static final int winner=10;
    private static final int win_by_runs=11;
    private static final int win_by_wickets=12;
    private static final int player_of_match=13;
    private static final int venue=14;




    private static final int match_id=0;
    private static final int inning=1;
    private static final int batting_team=2;
    private static final int bowling_team=3;
    private static final int over=4;
    private static final int ball=5;
    private static final int batsman=6;
    private static final int non_striker=7;
    private static final int bowler=8;
    private static final int is_superover=9;
    private static final int wide_runs=10;
    private static final int bye_runs=11;
    private static final int leg_bye_runs=12;
    private static final int noball_runs=13;
    private static final int penalty_runs=14;
    private static final int batsman_runs=15;
    private static final int extra_runs=16;
    private static final int total_runs=17;
    private static final int player_dissmissal=18;
    private static final int dissmissal_kind=19;
    private static final int fielder=20;




    public static void main(String[] args)
    {

        List<Match_Data> matches = new ArrayList<>();
        List<Delivery_Data> deliveries = new ArrayList<>();
        Problem1 obj1 = new Problem1();
        Problem2 obj2 = new Problem2();
        Problem3 obj3 = new Problem3();
        Problem4 obj4 = new Problem4();
        WinMatchAndToss obj5 = new WinMatchAndToss();

        try {
            FileReader fr = new FileReader("deliveries.csv");
            BufferedReader br = new BufferedReader(fr);
            String line="";
            while ((line = br.readLine()) != null) {
                 String[] data = line.split(",");
                 Delivery_Data delivery = new Delivery_Data();

                 //  Set All deliveries Data values through the setter method
                 delivery.setMatch_id(data[match_id]);
                 delivery.setInning(data[inning]);
                 delivery.setBatting_team(data[batting_team]);
                 delivery.setBowling_team(data[bowling_team]);
                 delivery.setOver(data[over]);
                 delivery.setBall(data[ball]);
                 delivery.setBatsman(data[batsman]);
                 delivery.setNon_striker(data[non_striker]);
                 delivery.setBowler(data[bowler]);
                 delivery.setIs_super_over(data[is_superover]);
                 delivery.setWide_runs(data[wide_runs]);
                 delivery.setBye_runs(data[bye_runs]);
                 delivery.setLegbye_runs(data[leg_bye_runs]);
                 delivery.setNoball_runs(data[noball_runs]);
                 delivery.setPenalty_runs(data[penalty_runs]);
                 delivery.setBatsman_runs(data[batsman_runs]);
                 delivery.setExtra_runs(data[extra_runs]);
                 delivery.setTotal_runs(data[total_runs]);

                 deliveries.add(delivery);
            }
            fr.close();
            br.close();
        }
        catch (Exception e) {
            System.out.println(e);
        }

        try {
            FileReader fr = new FileReader("matches.csv");
            BufferedReader br = new BufferedReader(fr);
            String line="";
            while ((line = br.readLine()) != null) {
                //System.out.print((String) line);
                String[] data = line.split(",");
                Match_Data match = new Match_Data();
                match.setId(data[id]);

                //  Set All Match Data values through the setter method
                match.setSeason(data[season]);
                match.setCity(data[city]);
                match.setDate(data[date]);
                match.setTeam1(data[team1]);
                match.setTeam2(data[team2]);
                match.setToss_winner(data[toss_winner]);
                match.setToss_decision(data[toss_decision]);
                match.setResult(data[result]);
                match.setDl_applied(data[dl_applied]);
                match.setWinner(data[winner]);
                match.setWin_by_runs(data[win_by_runs]);
                match.setWin_by_wickets(data[win_by_wickets]);

                matches.add(match);
            }
            br.close();
            fr.close();
        }
        catch (Exception e) {
            System.out.println(e);
        }

//      calling function and display Output
        System.out.println(obj1.findNumberOfMatchesPerYear(matches));
        System.out.println(obj2.findWonMatchesByEachTeam(matches));
        System.out.println(obj3.extraruns(matches,deliveries));

        List<Map.Entry<String, Double>> top10EconomyBowlers = obj4.findTopEconomicalBowlers(matches,deliveries);
        for(int i=0; i<=9; i++) {
            System.out.println(top10EconomyBowlers.get(i));
        }

        System.out.println(obj5.wonMatchAndAlsoWonToss(matches));

    }

}
//{=0, Mumbai Indians=47, Sunrisers Hyderabad=17, Pune Warriors=3, Rajasthan Royals=34, Kolkata Knight Riders=38, Royal Challengers Bangalore=25, Gujarat Lions=9, winner=0, Rising Pune Supergiant=4, Kochi Tuskers Kerala=3, Kings XI Punjab=27, Deccan Chargers=18, Delhi Daredevils=28, Rising Pune Supergiants=3, Chennai Super Kings=41}

