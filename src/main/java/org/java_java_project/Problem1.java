package org.java_java_project;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


//1. Number of matches played per year of all the years in IPL.
public class Problem1 {

    public static Map<String,Integer> findNumberOfMatchesPerYear(List<Match_Data> matches){
        Map<String, Integer> eachYear = new HashMap<>();
        for(Match_Data index : matches) {
            if (eachYear.containsKey(index.getSeason())) {
                int winCount = eachYear.get(index.getSeason());
                eachYear.replace(index.getSeason(), winCount + 1);
            } else {
                eachYear.put(index.getSeason(), 1);
            }
        }
        return eachYear;
    }

}
